# TP Devops

 Mathéo Colas / Loïc Christolomme

# Documentation

## Demarche

L'objectif de ce tp est de mettre en oeuvre une chaine d'intégration et de déploiement continu. Cette chaine permet d'automatiser les tests effectués sur le developpement d'un programme et de le mettre à disposition des utilisateurs automatiquement. Pour ce faire, nous allons dans un premier temps créer une image Docker, permettant d'uniformiser l'environnement d'éxecution du programme, ce qui veut dire que le fonctionnement sera exactement le même sur la machine du developpeur que sur le serveur de procution afin d'éviter les conflits liés au système.
Nous allons également utiliser GitLab qui vas nous permettre de créer  automatiquement cette image Docker, de jouer les tests, et si le résultat est positif, l'image sera envoyée sur un serveur Heroku, qui installera automatiquement l'application en production.

## Docker

### Presentation

Un conteneur est composé d'environnement d'exécution complet: une application, ses dépendances, ses libraires, sans oublier 
 les fichiers de configuration nécessaires au bon fonctionnement de l'application, regroupés dans une seule entité.

Docker est un logiciel libre à mi-chemin entre la virtualisation applicative et l'automatisation. Il permet de manipuler des conteneurs de logiciels. 

### Dockerfile

Les Dockerfiles sont des fichiers qui permettent de construire une image Docker, étape par étape.

Syntaxe du dockerfile

```
FROM node:12
```
FROM indique l'image de base du conteneur.

```
WORKDIR /app
```
WORKDIR indique le dossier à créer dans lequel éxécuter toutes les commandes necéssaire à la création de l'image.

```
COPY ./host /app
```
COPY indique les fichier présents sur l'hôte, à copier dans l'image docker (fichier sources du programme, librairies...).

```
RUN npm install
```
Execute les commandes nécéssaires à la creation de l'image.

```
CMD npm start
```
Cette commande ne peut être utilisée qu'une seule fois, elle lance l'éxecution de l'application liée au conteneur, lorsque cette commande est terminée, le conteneur s'éteint automatiquement.


### Commandes de base

Construction d'une image docker à partir d'un dockerfile

```
docker build -t nom:version
```

Lancement du conteneur à partir de l'image docker
```
docker run -p porthote:portconteneur nom:version
```

Lancement de commandes à l'intérieur du conteneur
```
docker exec -it nomconteneur:version /bin/sh 
```



### CI/CD

Les pipelines GitLab CI / CD sont configurés à l'aide d'un fichier YAML appelé `.gitlab-ci.yml` dans chaque projet.

Le fichier `.gitlab-ci.yml` définit la structure et l'ordre des pipelines.

Les pipelines constituent le composant de premier niveau de l'intégration, de la livraison et du déploiement continus.

Dans ce projet chaque pipeline éxécutera 3 étapes séquentielle build > test > deploy.

```
stages:
  - build
  - test
  - deploy
```


##### BUILD

Ce job lance la création de l'image docker à partir du dockerfile présent dans le dépot gitlab, puis envoie l'image frainchement crée dans le dépot d'images Docker intégré à Gitlab.

```
job-build:
  stage: build
  script:
    - docker build -t $CONTAINER_TEST_IMAGE .
    - docker push $CONTAINER_TEST_IMAGE 
```


##### TEST

Une fois que l'étape build est terminé, on réalise l'étape de TEST. Pour ce faire on récupère l'image Docker qui a été push sur le container. Ensuite on réalise la commande de test qui va exécuter les fichier se trouvant dans le répertoire test. Si les tests échoues alors le job l'intégration s'arrête.

```
 job-test:
    stage: test
    script:
      - docker pull $CONTAINER_TEST_IMAGE
      - docker run $CONTAINER_TEST_IMAGE npm run test --watch --rm
```

##### DEPLOY

Le déploiment ce réalise sur un serveur HEROKU. 

Heroku est une plateforme Saas qui permet notament de déployer des serveurs gratuitement, basés sur une image docker.

Le déploiement consiste en 3 étapes:
* construire une image docker exécutant le serveur.
* pousser cette image docker dans le repository heroku `registry.heroku.com`.
* publier (“release”) cette image pour que le container serveur redémarre en l’utilisant.


Dans les faits il faut : 
- Une fois encore ont récupère l'image Docker.
- Il faut tout d’abord “tagger” l’image avec le nom du registry Heroku, le nom de l’application visée, et le type de process - web dans notre cas. 
- Ensuite il faut se login sur le registry Heroku, grâce à la clef d’API.
- Ensuite on peut simplement pousser l’image dans le repository Heroku.
- Et pour finir, ont publie l'image Docker sur HEROKU
```
 job-deploy:
    stage: deploy
    script:
      - docker pull $CONTAINER_TEST_IMAGE
      - docker tag $CONTAINER_TEST_IMAGE registry.heroku.com/cia-devop/web
      - docker login --username=_ --password=$HEROKU_AUTH_TOKEN registry.heroku.com
      - docker push registry.heroku.com/cia-devop/web
      - docker run --rm -e HEROKU_API_KEY=$HEROKU_AUTH_TOKEN wingrunr21/alpine-heroku-cli container:release web --app cia-devop
```
La variable HEROKU_AUTH_TOKEN est à configurer dans gitlab avec l'API key de votre compte HEORKU.


