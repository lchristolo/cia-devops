exports.order = function order(req, res, next) {
  // TODO implement from here
  //price quantites country/state reduction 
  console.log(req.body);
  if(req.body.prices != null && req.body.quantities!=null && req.body.quantities.lenght == req.body.prices.lenght){
    if(req.body.country in europeanCountriesTaxes){
      var sum = 0
      for (let index = 0; index < req.body.quantities.length; index++) {
        sum+=(req.body.quantities[index]*req.body.prices[index])
      }
      taxrate = europeanCountriesTaxes[req.body.country]
      
      if(req.body.reduction == 'PAY THE PRICE'){
        res.json({'total':(sum)*taxrate});
      }
      else if(req.body.reduction = 'HALF PRICE'){
        res.json({'total':(sum/2)*taxrate});
      }
      else if(req.body.reduction = 'STANDARD'){
        var reduced = 0
        sum = sum * taxrate;
        if(sum => 0 && sum <1000){
          sum = sum * 1 ;
        }
        else if (sum=>1000 && sum <5000){
          sum= sum  *0.97;
        }
        else if (sum =>5000 && sum<7000){
          sum = sum *0.95;
        }
        else if (sum=>7000 && sum<10000){
          sum = sum * 0.93;
        }
        else if (sum=>10000 && sum<50000){
          sum = sum*0.9;
        }
        else if (sum=>50000) {
          sum = sum * 0.85;
        }
        res.json({'total':sum});
      }
      else {
        res.statusMessage = "Bad request";
        res.status(400).end();
      }

    }
  }

  
};

exports.feedback = function feedback(req, res, next) {
  
  console.info("FEEDBACK:", req.body.type, req.body.content);
  next();
};

const europeanCountriesTaxes = {
  'DE': 1.2,
  'UK': 2,
  'FR': 0.5,
  'IT': 1.25,
  'ES': 1.19,
  'PL': 1.21,
  'RO': 1.2,
  'NL': 1.2,
  'BE': 1.24,
  'EL': 1.2,
  'CZ': 1.19,
  'PT': 1.23,
  'HU': 1.27,
  'SE': 1.23,
  'AT': 1.22,
  'BG': 1.21,
  'DK': 1.21,
  'FI': 1.17,
  'SK': 1.18,
  'IE': 1.21,
  'HR': 1.23,
  'LT': 1.23,
  'SI': 1.24,
  'LV': 1.2,
  'EE': 1.22,
  'CY': 1.21,
  'LU': 1.25,
  'MT': 1.2
};

const standardReductions = [
  { startingPrice: 0, rate: 0 },
  { startingPrice: 1000, rate: 0.03 },
  { startingPrice: 5000, rate: 0.05 },
  { startingPrice: 7000, rate: 0.07 },
  { startingPrice: 10000, rate: 0.1 },
  { startingPrice: 50000, rate: 0.15 },
];
